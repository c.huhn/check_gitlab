# changes by release

## 0.1.1
* using v4 api endpoint
* health mode: using readiness probes endpoint instead of deprecated health_check - https://docs.gitlab.com/ce/user/admin_area/monitoring/health_check.html#using-the-endpoint

### fixes
* pipeline mode: only reports with status 'success'. running and skipped pipelines will not generate an error anymore.

## 0.1
* Initial release
