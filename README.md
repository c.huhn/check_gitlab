# check_gitlab
[![build status](https://gitlab.com/6uellerBpanda/check_gitlab/badges/master/build.svg)](https://gitlab.com/6uellerBpanda/check_gitlab/commits/master)

Gitlab Naemon/Icinga/Nagios plugin which checks various stuff via Gitlab API(v4) and output of gitlab-ctl services.

Tested with: Naemon 1.0.5, 1.0.6; Ruby 2.3.0, 2.3.3

# Usage
```shell_session
./check_gitlab
check_gitlab v0.1.1 [https://gitlab.com/6uellerBpanda/check_gitlab]

This plugin checks various parameters of Gitlab

Mode:
  health       Check the Gitlab web endpoint for health
  services     Check if any service of 'gitlab-ctl status' is down
  ci-pipeline  Check duration of a CI pipeline
  ci-runner    Check status of CI runners

Usage: check_gitlab [options]

Options:
    -s, --address ADDRESS            Gitlab address
    -t, --token TOKEN                Access token
    -i, --id ID                      Project ID
    -k, --insecure                   No ssl verification
    -m, --mode MODE                  Mode to check
    -w, --warning WARNING            Warning threshold
    -c, --critical CRITICAL          Critical threshold
    -v, --version                    Print version information
    -h, --help                       Show this help message
```

## Options
-s: gitlab url, only https supported, https://gitlab.example.com

-k: if you've a self signed cert

-t: access token, required for all api calls

-i: project id, number

# Modes
## Health check
Checks the status of the readiness endpoint.
See https://docs.gitlab.com/ce/user/admin_area/monitoring/health_check.html for more information.
```shell_session
naemon@gitlab:plugins$ ./check_gitlab -m health -s <gitlab_url> [-k] -t <access_token>
OK - Gitlab probes are in healthy state
```

## Services
If any service of 'gitlab-ctl status' reports down service status will be critical.

Requires a sudo entry to get output of gitlab-ctl command.

```conf
naemon ALL = NOPASSWD: /usr/bin/gitlab-ctl status
```

```shell_session
naemon@gitlab:plugins$ ./check_gitlab -m services
Critical - logrotate, mattermost is down
```

## CI Pipeline
Checks duration of CI pipeline in seconds with perfdata.

```shell_session
naemon@gitlab:plugins$ ./check_gitlab -m ci-pipeline -s <gitlab_url> [-k] -t <access_token> -i <procject_id> -w 100 -c 200
Critical - Pipeline #265 took 265s | duration=265s;100;150
```

## CI Runner
Checks status of CI runner.

```shell_session
naemon@gitlab:plugins$ ./check_gitlab -m ci-runner -s <gitlab_url> [-k] -t <access_token> -i <procject_id> -w 0 -c 1
Warning - 1 runner active, 1 runner not active
```
